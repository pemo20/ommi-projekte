# OMMI-Projekte für das Modul VWS im WS 23/24


## ESA3

Kleine Python- und C#-Programme für das Erfassen von Standard-Codemetriken

## ESA4

Die Umsetzung des klassischen Streichholzspiels als Beispiele für Clean Code und "Not so clean Code" (NSCC)

## ESA7

Ein sehr kleiner "Microservice", der als Flask App ein paar sehr einfache Quizze zur Verfügung stellt.

## ESA7A

Weiterer "Microservice", der als FastAPI App die Auswahl von Quizzen durch ESA7 über ein Benutzerprofile ermöglicht.

ESA7 und ESA7A können auch als Docker Container ausgeführt werden.

## ESA7B

Ein auf der DigitalOcean-Plattform umgesetzter serverless Code in Gestalt einer in Python implementierten Function, die als klassischer QoD-Service ein Zitat zurückgibt. Über den Parameter *Category* wird eine Kategorie vorgegeben, z.B. "STOS" für StarTrek Original Series.

Der Quellcode der Function:

```python

# created 10/01/24
import random
import requests

quotes = [
    "When it doesn't work the first time, give up",
    "Never to late to stay in bed",
    "Don't worry, be lucky",
    "Work hurts"
]

def getLocalQuote() -> str:
    z = random.randint(0, len(quotes)-1)
    return quotes[z]

def getRemoteQuote(category) -> str:
    response = requests.get(apiUri)
    lines = [l.strip() for l in response.text.split("\n") if l.strip() != ""]
    quoteDic = {}
    lastKey = ""
    for line in lines:
        if line.endswith(":"):
            lastKey = line[:-1]
            quoteDic[lastKey] = []
        else:
            quoteDic[lastKey].append(line)
    if quoteDic.get(category) != None:
        z = random.randint(0, len(quoteDic[category]) -1)
        quote = quoteDic[category][z]
    else:
        quote = f"Sorry, no quotes for this category ({category})!"
    return quote


def main(args):
    try:
        category = args.get("category", "General")
        quote = getRemoteQuote(category)
    except Exception as ex:
        print(f"!!! error in main: {ex}")
        quote = getLocalQuote()
    print(quote)
    return {"body": quote}

```

Die Adresse der Function ist

https://faas-fra1-afec6ce7.doserverless.co/api/v1/web/fn-27efee14-6129-447d-a2d8-819a908102f7/default/getQoD

Aufruf z.B. per PowerShell (auch unter Linux und MacOS) per **Invoke-WebRequest** (Alias _Curl_):

Invoke-WebRequest -Uri https://faas-fra1-afec6ce7.doserverless.co/api/v1/web/fn-27efee14-6129-447d-a2d8-819a908102f7/default/getQoD?category=STOS


StatusCode        : 200
StatusDescription : OK
Content           : Doctor Mc Coy, you may now cure the common cold
RawContent        : HTTP/1.1 200 OK
                    Transfer-Encoding: chunked
                    Connection: keep-alive
                    X-Request-ID: 80b82805372bc686534922d4a664cabd
                    Access-Control-Allow-Origin: *
                    Access-Control-Allow-Methods: OPTIONS, GET, DELETE,...
Forms             : {}
Headers           : {[Transfer-Encoding, chunked], [Connection, keep-alive], [X-Request-ID,
                    80b82805372bc686534922d4a664cabd], [Access-Control-Allow-Origin, *]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 47


## ESA8A

Python-Projekt, das mit Hilfe der Antlr4 runtime einen Expression Evaluator für UPN-Ausdrücke umsetzet.

## ESA8B

PowerShell-Modul, das eine sehr einfache DSL für die Definition von Quizzen definiert. Der Output ist eine XML- oder eine YAML-Datei.

Um das Skript ausprobieren zu können, müssen die Psm1- und die Ps1-Datei in ein Verzeichnis kopiert werden, z.B. C:\Temp

Dann in der PowerShell-Konsole:

```
cd C:\Temp
```
und 

```
.\Quiz01.Ps1
```

Das Ergebnis ist eine Xml-Datei im aktuellen Verzeichnis.

Soll stattdessen eine Yaml-Datei erzeugt werden, muss in der Ps1-Datei beim Aufruf von Quiz am Ende ein "-AsYaml" angehängt werden.

Sollte es eine Fehlermeldung geben, muss wahrscheinlich die PowerShell-Ausführungsrichtlinie noch aktualisiert werden, z.B.

```
Set-ExecutionPolicy Unrestricted -Scope CurrentUser
```

## ESA8C

Eine etwas bessere Version, in der die Quiz-Datei entweder aus reinem Text besteht oder ein PowerShell DATA Element enthält.

Bei dieser Umsetzung wird das Quiz "zustandsbezogen" verarbeitet, d.h. ein _question_-Element bezieht sich immer auf ein bereits vorhandendes _Quiz_.Element. Ein potentieller Quiz-Autor muss daher keine PowerShell-Syntax berücksichtigen bzw. bei Verwendung des _DATA-Elements gibt es nur einen Rahmen, aber keine Verschachaltung. 

Dies ist die bessere Usmetzung.

## ESA10

Ich habe ein kleines Zahlenratespiel mit F# umgesetzt. F# ist eine hybride funktionale Programmiersprache als Teil von .Net, die sowohl den funktionalen Ansatz als auch den klassischen imperativen Ansatz unterstützt. Ich finde diesen Ansatz sehr gut, da die Umstellung auf einen rein funktionalen Programmierstil für viele Entwickler nicht attraktiv genug sein dürfte, da der Lernaufwand groß und der "Benefit" doch etwas abstrakt ist.

Das kleine Spiel erzeugt eine Zufallszahl und ermöglicht die Eingabe einer Zahl. Als Antwort wird "höher", "niedriger" oder erraten ausgegeben.

Mit der Zufallszahl, der aktuellen Eingabe und der Anzahl der Rateversuche gibt es drei Zustände.

Eine kleine Verbesserung wäre, dass die aktuelle Ratezahl in der Eingabeaufforderung ausgegeben wird.

