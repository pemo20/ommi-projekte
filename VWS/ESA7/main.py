# file: Main.py

from flask import Flask
from app import app
from os import path

template_dir = path.join(path.dirname(__file__), "templates")
  
if __name__ == "__main__":
  app = Flask(__name__,template_folder=template_dir)
  app.run(debug=True)
