#! env/bin/python3
# file: quizHelper.py

import logging
from app.quiz import Quiz
import yaml

logging.basicConfig(filename="quizserver.log", level=logging.INFO, format=f"%(asctime)s %(levelname)s %(name)s : %(message)s")

def getQuizMetadata(filepath) -> Quiz:
    logging.info(f"*** calling getQuizMetdata with path={filepath}")
    try:
        with open(filepath, encoding="utf8") as fh:
            quizData = yaml.safe_load(fh)
            quizId = quizData["quiz"]["id"]
            quizTitle = quizData["quiz"]["title"]
            metaDic = {}
            metaDic["author"] = quizData["quiz"]["author"]
            metaDic["lang"] = quizData["quiz"]["lang"]
            metaDic["createDate"] = quizData["quiz"]["createDate"]
            metaDic["level"] = quizData["quiz"]["level"]
            metaDic["category"] = quizData["quiz"]["category"]
            return Quiz(quizId, quizTitle, **metaDic)
        return quizData
    except Exception as ex:
        logging.error(f"!!! getQuizMetdata ex={ex}")
