# file: views.py
import os

from flask import render_template, send_from_directory
from app import app 
from app.quizHelper import getQuizMetadata
import logging
import requests
import json

logging.basicConfig(filename="quizserver.log", level=logging.INFO, format=f"%(asctime)s %(levelname)s %(name)s : %(message)s")

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, "static"),
            "favion.ico", mimetype="image/vnd.microsoft.icon")

@app.route('/')
def getProfileQuizze():
    app.logger.info("*** calling getProfileQuizze ***")
    userId = 1000
    api_url = f"http://localhost:8090/api/profile/{userId}"
    try:
        response = requests.get(api_url)
        responseJson = response.json()
        resultJson = responseJson["result"]
        profile = json.loads(resultJson)
        profileLevel = profile["level"]
        quizList = []
        quizPath = os.path.join(app.root_path, "static", "data")
        for quizFile in [fi for fi in os.listdir(quizPath) if fi.endswith("yml")]:
            quiz = getQuizMetadata(os.path.join(quizPath, quizFile))
            if quiz.level == profileLevel:
                quizList.append(quiz)
        app.logger.info(f"*** Getting {len(quizList)} quiz for userId={userId}")
    except Exception as ex:
        logMsg = f"!!! error calling profile service {ex}"
        logging.error(logMsg)
        app.logger.error(logMsg)
        quizList = []
    return render_template('home.html', quizzes=quizList)

@app.route("/api/quiz")
def getQuizze():
    app.logger.info("*** calling getQuizze ***")
    quizList = []
    quizPath = os.path.join(app.root_path, "static", "data")
    for quizFile in [fi for fi in os.listdir(quizPath) if fi.endswith("yml")]:
        quiz = getQuizMetadata(os.path.join(quizPath, quizFile))
        quizList.append(quiz)
    return render_template('quizlist.html', quizzes=quizList)

@app.route("/api/quiz/<id>")
def getQuizz(id):
    app.logger.info(f"*** calling getQuiz with id={id} ***")
    quizPath = os.path.join(app.root_path, "static", "data")
    quizFile = [fi for fi in os.listdir(quizPath) if fi == f"quiz_{id}.yml"]
    if len(quizFile) > 0:
        quizFile = quizFile[0]
        quiz = getQuizMetadata(os.path.join(quizPath, quizFile))
    return render_template("quizdetails.html", quiz=quiz)