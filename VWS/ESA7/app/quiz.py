#! env/bin/python3
# file: Quiz.py

class Quiz:

    def __init__(self, id, title, **kwargs) -> None:
        self.id = id
        self.title = title
        if kwargs.get("author"):
            self.author = kwargs["author"]
        if kwargs.get("lang"):
            self.lang = kwargs["lang"]
        if kwargs.get("createDate"):
            self.createDate = kwargs["createDate"]
        if kwargs.get("level"):
            self.level = kwargs["level"]
        if kwargs.get("category"):
            self.category = kwargs["category"]

    def __repr__(self) -> str:
        return f"Id: {self.id} Title: {self.title} Category {self.category}"