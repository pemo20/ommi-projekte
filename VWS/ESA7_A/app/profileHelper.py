# file: profileHelper.py
#! env/bin/python3

from os import path
import yaml
import json

class ProfileHelper:

    '''
    returns a dictionary with profile data for a specific id
    '''
    def getProfile(self, id) -> object:
        ymlPath = path.join(path.dirname(__file__), "profiles.yaml")
        with open (ymlPath, mode="r", encoding="utf-8") as fh:
            profiles = yaml.safe_load(fh)
        # get the profile
        # better with chain from itertools?
        # does not work
        # results = [p for p in profiles.values() if p["id"] == str(id)]
        profile = None
        for pDic in profiles.values():
            if pDic["id"] == id:
                profile = pDic
                break
        if profile is not None:
            jsonData = json.dumps(profile)
            return jsonData
        else:
            return {"result" : -1}
        
