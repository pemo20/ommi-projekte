# file: main.py
#! env/bin/python3
# Start mit uvicorn app.main:app --host 0.0.0.0 --port 8090 --reload

from typing import Union
from fastapi import FastAPI,HTTPException
from app.profileHelper import ProfileHelper
import logging

app = FastAPI()

@app.get("/")
def getInfo():
    return {"version" : 0.1}

@app.get("/api/profiles")
def get_profiles() -> []:
    pass

@app.get("/api/profile/{profileId}")
def get_profile(profileId : int) -> object:
    try:
        helper = ProfileHelper()
        profile = helper.getProfile(profileId)
    except Exception as ex:
        logging.error(f"!!! get_profile - error {ex}")
        profile = -1
    result = {"result": profile}
    return result