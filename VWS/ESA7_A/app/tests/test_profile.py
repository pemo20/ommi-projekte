# file: test_profile-py

import sys
from fastapi.testclient import TestClient
from os import path

currentDir = path.dirname(__file__)
parentDir = path.dirname(currentDir)
sys.path.insert(0, parentDir)

from profileHelper import ProfileHelper
from main import app

client = TestClient(app)

def test_profile1():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"version" : 0.1}
    