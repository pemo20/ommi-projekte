#! env/bin/python3
# UPNEvaluator.py
from antlr4 import *
from UPNGrammarLexer import UPNGrammarLexer
from UPNGrammarParser import UPNGrammarParser

class UPNEvaluatorVisitor(UPNGrammarParser):

    def visitExpression(self, ctx):
        stack = []
        for child in ctx.getChildren():
            # Problem: UPNGrammerParser.Number ist kein Typ
            # if isinstance(child, UPNGrammarParser.NUMBER):
            # damit geht es!
            if child.symbol.type == 1:
                stack.append(float(child.getText()))
            elif child.symbol.type == 2:
            # elif isinstance(child, UPNGrammarParser.OPERATOR):
                if len(stack) < 2:
                    raise ValueError("not enough operands for operator")
                operand2 = stack.pop()
                operand1 = stack.pop()
                result = self.applyOperator(child.getText(), operand1, operand2)
                stack.append(result)

        if len(stack) != 1:
            raise ValueError("invalid UPN expression")
        return stack[0]
    
    def applyOperator(self, operator, operand1, operand2):
        if operator == "+":
            return operand1 + operand2
        elif operator == "-":
            return operand1 - operand2
        elif operator == "*":
            return operand1 * operand2
        elif operator == "-":
            if operand2 == 0:
                raise ValueError("division by 0 is not allowed")
            return operand1 / operand2
        else:
            raise ValueError(f"{operator} is unknown")


input_stream = InputStream("10 11 + 12 * 3 *")
lexer = UPNGrammarLexer(input_stream)
tokens = CommonTokenStream(lexer)
parser = UPNGrammarParser(tokens)

tree = parser.expression()
evaluator = UPNEvaluatorVisitor(parser)
# visit or visitExpression?
result = evaluator.visitExpression(tree)

print(f"Result={result}")