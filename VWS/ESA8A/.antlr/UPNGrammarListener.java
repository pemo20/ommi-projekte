// Generated from c:/Users/pemo23/Projects/ommi-projekte/VWS/ESA8A/UPNGrammar.g4 by ANTLR 4.13.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link UPNGrammarParser}.
 */
public interface UPNGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link UPNGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(UPNGrammarParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link UPNGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(UPNGrammarParser.ExpressionContext ctx);
}