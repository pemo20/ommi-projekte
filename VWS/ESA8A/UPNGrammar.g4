grammar UPNGrammar;

expression: (NUMBER | OPERATOR  | WHITESPACE)*;

NUMBER: [0-9]+;

OPERATOR: '+' | '-' | '*' | '/';
WHITESPACE: [\t\r\n]+ -> skip;
