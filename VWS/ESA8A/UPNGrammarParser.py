# Generated from UPNGrammar.g4 by ANTLR 4.9.3
# encoding: utf-8
# Wichtig: runtime muss zur Antrl version passen, also z.B. pip install antlr4-python3-runtime==4.9.3
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\5")
        buf.write("\13\4\2\t\2\3\2\7\2\6\n\2\f\2\16\2\t\13\2\3\2\2\2\3\2")
        buf.write("\2\3\3\2\3\5\2\n\2\7\3\2\2\2\4\6\t\2\2\2\5\4\3\2\2\2\6")
        buf.write("\t\3\2\2\2\7\5\3\2\2\2\7\b\3\2\2\2\b\3\3\2\2\2\t\7\3\2")
        buf.write("\2\2\3\7")
        return buf.getvalue()


class UPNGrammarParser ( Parser ):
    grammarFileName = "UPNGrammar.g4"
    atn = ATNDeserializer().deserialize(serializedATN())
    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]
    sharedContextCache = PredictionContextCache()
    literalNames = [  ]
    symbolicNames = [ "<INVALID>", "NUMBER", "OPERATOR", "WHITESPACE" ]
    RULE_expression = 0
    ruleNames =  [ "expression" ]
    EOF = Token.EOF
    NUMBER=1
    OPERATOR=2
    WHITESPACE=3

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.3")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None


    class ExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self, i:int=None):
            if i is None:
                return self.getTokens(UPNGrammarParser.NUMBER)
            else:
                return self.getToken(UPNGrammarParser.NUMBER, i)

        def OPERATOR(self, i:int=None):
            if i is None:
                return self.getTokens(UPNGrammarParser.OPERATOR)
            else:
                return self.getToken(UPNGrammarParser.OPERATOR, i)

        def WHITESPACE(self, i:int=None):
            if i is None:
                return self.getTokens(UPNGrammarParser.WHITESPACE)
            else:
                return self.getToken(UPNGrammarParser.WHITESPACE, i)

        def getRuleIndex(self):
            return UPNGrammarParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)


    def expression(self):

        localctx = UPNGrammarParser.ExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_expression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 5
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << UPNGrammarParser.NUMBER) | (1 << UPNGrammarParser.OPERATOR) | (1 << UPNGrammarParser.WHITESPACE))) != 0):
                self.state = 2
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << UPNGrammarParser.NUMBER) | (1 << UPNGrammarParser.OPERATOR) | (1 << UPNGrammarParser.WHITESPACE))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 7
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx
