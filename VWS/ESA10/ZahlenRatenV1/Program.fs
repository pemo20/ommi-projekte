﻿// Ein einfaches Zahlenratespiel mit F#

open System

// Holt eine Zufallszahl zwischen 1 und 1024
let getRandomNumber =
    let rnd = new Random()
    rnd.Next(1, 1024)

// Eine echte Variable
let mutable guessCounter = 0

// Hier beginnt die rekursive Funktion
let rec guessNumber number = 
    printfn "Gib eine Zahl zwischen 1 und 1024 ein (q für Ende):"
    // Überprüfen der Eingabe durch Pattern Matching
    match Console.ReadLine() with
    | s when s.ToLower() = "q" -> printfn "Auf Wiedersehen!"
    | numberInput -> 
        // Erhöhen des Versuchs-Zählers
        guessCounter <- guessCounter + 1
        // Überprüfen der Eingabe durch Pattern Matching
        match Int32.TryParse(numberInput) with
        | true, numberGuess when numberGuess = number ->
            printfn $"Du hast die Zahl nach {guessCounter} Versuchen erraten!"
        | true, numberGuess when numberGuess < number -> 
            printfn "Die gesuchte Zahl ist größer!"
            // Rekursiver Aufruf der Funktion
            guessNumber number
        | true, numberGuess when numberGuess > number ->
            printfn "Die gesuchte Zahl ist kleiner!"
            // Rekursiver Aufruf der Funktion
            guessNumber number
        | _ ->
            printfn "Ungültige Eingabe. Gib eine ganze (!) Zahl ein!"
            // Rekursiver Aufruf der Funktion
            guessNumber number

// Einsprungsfunktion
let startGame = 
    printfn "Willkommen beim wilden Zahlenraten!"
    printfn "Ich habe mir eine Zahl zwischen 1 und 1024 ausgedacht."
    printfn "Errate sie oder stirb!"
    let randomNumber = getRandomNumber
    guessNumber randomNumber

// Offizieller Programmstart
[<EntryPoint>]
startGame