#! env/usr/bin/python3
# file: EinzahlMehrzahlConverter.py

# Konvertiert ein Wort in die richtige Einzahl-/Mehrzahlform
class EMConverter:

      # gibt das richtige Einzahl/Mehrzahlwort zurück
    def getEinzahlMehrzahlWort(self, kategorie, anzahl) -> str:
        dicKategorien = {"Streichholz": ("Streichholz", "Streichhölzer"),
                         "Anzahl": ("ein", "eine"),
                          "Liegen": ("liegt", "liegen"),
                          "sind": ("ist", "sind")
                        }
        # Wortformen holen
        wortAuswahl = dicKategorien[kategorie]
        # Wortform zuordnen
        return wortAuswahl[0] if anzahl == 1 else wortAuswahl[1]