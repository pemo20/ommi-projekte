// file: Drawer.cs

using System.Collections.Generic;

namespace OCP_Bad
{

  public static class Drawer
  {
     public static void drawShapes(IEnumerable<Object> shapes) {
        foreach (var shape in shapes) {
           switch (shape.GetType().Name) {
             case "Circle":
                Console.WriteLine("Drawing a Circle...");
                break;
             case "Square":
                Console.WriteLine("Drawing a Square...");
                break;
             default:
                Console.WriteLine("Unknown object...");
                break;
           }
        }
     }
  }
}