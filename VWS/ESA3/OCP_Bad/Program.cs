﻿// file: Program.cs

using System.Collections.Generic;

namespace OCP_Bad
{

  class Program
  {

    public static void Main(String[] args) {
       var drawObjects = new List<Object>{new Circle(),new Circle(), new Square()};
       Drawer.drawShapes(drawObjects);
    }
  }
 
 }