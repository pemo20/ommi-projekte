﻿// file: Program.cs

using System.Collections.Generic;

namespace OCP_Good
{

  class Program
  {

    public static void Main(String[] args) {
       var drawObjects = new List<Shape>{new Circle("Circle"), new Circle("Circle"), new Square("Square")};
       var retVal = Drawer.drawShapes(drawObjects);
       Console.WriteLine(retVal);
    }
  }
 
 }