// file: Drawer.cs

using System.Collections.Generic;

namespace OCP_Good
{

  public static class Drawer
  {
     public static int drawShapes(IEnumerable<Shape> shapes) {
         var shapeCount = 0;
         foreach (var shape in shapes) {
            shapeCount += shape.draw() ? 1 : 0;
         }
         return shapeCount;
     }
  }
}