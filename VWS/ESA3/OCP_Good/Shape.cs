// file: Draw.cs
// Durch die gemeinsame Basisklasse soll dem Single Responsiblity-Prinzip Rechnung getragen werden
// Die einzelnen Shape-Klassen haben dadurch nur eine Resposibility - sie selbst zu sein;)

namespace OCP_Good
{

  public class Shape : IDraw
  {
    public string shapeType {get; set;}

    public Shape(string Type) {
      this.shapeType = Type;
    }

    public Boolean draw()
    {
        Console.WriteLine($"Drawing a nice {this.GetType().Name}...");
        return true;
    }

  }
  
}