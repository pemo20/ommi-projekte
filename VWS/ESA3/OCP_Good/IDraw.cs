// file: IDraw.cs

namespace OCP_Good
{

  interface IDraw
  {
    Boolean draw();
  }
  
}