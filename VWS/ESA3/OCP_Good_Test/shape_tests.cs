// file: shape_tests.cs
// some very simple tests for test coverage

using System.Collections.Generic;

namespace OCP_Good_Test;

using OCP_Good;

[TestClass]
public class SimpleShapeTests
{
    [TestMethod]
    public void TestMethod1()
    {
        var shapes = new List<Shape>{new OCP_Good.Circle("Kreis 1"), new OCP_Good.Circle("Kreis 2"), new OCP_Good.Square("Rechteck 1")};
        var count = OCP_Good.Drawer.drawShapes(shapes);
        Assert.IsTrue(count == 3);
    }


}