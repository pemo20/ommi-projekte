<#
 .SYNOPSIS
 Mini-DSL für Quizze in PowerShell
#>

class question
{
    [String]$Text
    [String[]]$Options

    question([String]$Text)
    {
        $this.Text = $Text
        $this.Options = @()
    }
}

class quiz
{
    [String]$Title
    [String]$Level
    [question[]]$Questions

    quiz([String]$Title, [String]$Level)
    {
        $this.Title = $Title
        $this.Level = $Level
        $this.Questions = @()
    }
}

function quiz
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][String]$Title,
        [Parameter(Mandatory=$true)][String]$Level
    )
    $Script:CurrentQuiz = [quiz]::new($Title, $Level)

}

function asxml
{
@"
<?xml version="1.0" encoding="utf-8"?>
<quiz title="$($Script:CurrentQuiz.Title)" level="$($Script:CurrentQuiz.Level)">
"@
    $Script:CurrentQuiz.Questions | ForEach-Object {
@"
  <question>
  <text>$($_.Text)</text>
"@
    $Script:CurrentQuestion.Options | ForEach-Object {
        if ($_.StartsWith("+"))
        {
            $_ = $_.Substring(1)
            "<option correct='true'>$($_)</option>"
        }
        else
        {
            "<option>$($_)</option>"
        }
    }
  "</question>"
    }
"</quiz>"
}

function asyaml
{
@"
quiz:
  title: $($Script:CurrentQuiz.Title)
  level: $($Script:CurrentQuiz.Level)
"@
$questionCounter = 0
    $Script:CurrentQuiz.Questions | ForEach-Object {
    @"
  question$((++$questionCounter)):
    text: $($_.Text)
    options:
"@
$optionsCounter = 0
$correct = ""
$_.Options | ForEach-Object {
    $text = "text$((++$optionsCounter))"
    if ($_.StartsWith("+")) {
        $correct = $text
        $_ = $_.Substring(1)
    }
    "        $($text): $($_)"
    }
    if ($correct -ne "") {
        "        correct: $correct"
    }
  }

}

function question
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][String]$Text
        )
        $Script:CurrentQuestion = [question]::new($Text)
        $Script:CurrentQuiz.Questions += $Script:CurrentQuestion
}

function options
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][String[]]$Options
    )
    $Script:CurrentQuestion.Options = $Options

}

