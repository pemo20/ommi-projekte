<#
 .SYNOPSIS
 Erstellt ein kleines Quiz mit der PowerShell Quiz DSL
#>

$psm1Path = Join-Path -Path $PSScriptRoot -ChildPath "QuizMaker.psm1"
Import-Module -Name $psm1Path -Force

# Verarbeiten einer reinen txt-Datei ohne PowerShell-Elemente
# Nicht optimal, da Invoke-Expression alles ausf�hrt, was in der Datei steht
$quizPath = Join-Path -Path $PSScriptRoot -ChildPath "quiz1.txt"
Get-Content -Path $quizPath -Encoding UTF8 | Invoke-Expression

# Jetzt als YAML
$quizPath = Join-Path -Path $PSScriptRoot -ChildPath "quiz2.txt"
Get-Content -Path $quizPath -Encoding UTF8 | Invoke-Expression

# Mit DATA geht es nicht, da z.B. keine Variablen erlaubt sind (siehe https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_data_sections?view=powershell-7.1)

# Quiz-Datei enth�lt ein DATA-Element und l�sst nur die Quiz-Commands zu
$quizPath = Join-Path -Path $PSScriptRoot -ChildPath "quiz3.txt"
Get-Content -Path $quizPath -Encoding UTF8 -Raw | Invoke-Expression
