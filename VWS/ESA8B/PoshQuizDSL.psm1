<#
 .SYNOPSIS
 Eine einfache DSL für die Definition von Quizzen
 .NOTES
 Letzte Aktualisierung 16/01/24
#>

# Setzt PowerShell ab Version 7.0 voraus (wegen ? :)

#requires -Version 7.0

enum OutputMode
{
    Xml
    Yaml
}

$Script:xmlText = ""
$Script:ymlText = ""
$Script:OutputMode = $null
$Script:QuestionCounter = 0
$Script:OptionCounter = 0

function Quiz
{
    [CmdletBinding(PositionalBinding=$false)]
    param([Parameter(Mandatory=$true)][String]$Id,
          [Parameter(Mandatory=$true)][String]$Title,
          [Parameter(Mandatory=$true)][String]$Level,
          [Parameter(Mandatory=$true)][String]$Description,
          [Parameter(Position=5)] [Scriptblock]$SB,
          [Parameter(Position=6)] [Switch]$AsYaml
          )
    if ($PSBoundParameters.ContainsKey("AsYaml")) {
        $Script:OutputMode = [OutputMode]::Yaml
        $Script:ymlText = "Quiz:`n"
        $Script:ymlText += $PSBoundParameters.ContainsKey("Title") ? "  title:$Title`n" : ""
        $Script:ymlText += $PSBoundParameters.ContainsKey("Level") ? "  level:$Level`n" : ""
        $Script:ymlText += $PSBoundParameters.ContainsKey("Description") ? "  description:$Description`n" : ""
        $Script:ymlText += "`n"
        $SB.Invoke()
        $ymlPath = Join-Path -Path $PSScriptRoot -ChildPath "quiz_$Id.yml"
        Set-Content -Path $ymlPath -Value  $Script:ymlText -Verbose
    } else {
        $Script:OutputMode = [OutputMode]::Xml
        $Script:xmlText = "<quiz"
        $Script:xmlText += $PSBoundParameters.ContainsKey("Title") ? " title='$Title'" : ""
        $Script:xmlText += $PSBoundParameters.ContainsKey("Level") ? " level='$Level'" : ""
        $Script:xmlText += $PSBoundParameters.ContainsKey("Description") ? " description='$Description'" : ""
        $Script:xmlText += ">`n"
        $SB.Invoke()
        $Script:xmlText += "</quiz>"
        $xmlPath = Join-Path -Path $PSScriptRoot -ChildPath "quiz_$Id.xml"
        Set-Content -Path $xmlPath -Value  $Script:xmlText -Verbose
    }
}

function Question
{
    param([Parameter(Mandatory=$true)][String]$QuestionText, 
          [Scriptblock]$SB
    )
    $Script:QuestionCounter++
    $Script:OptionCounter = 0
    # Welcher Modus?
    switch($Script:OutputMode)
    {
        Xml {
            $Script:xmlText += "<question id='$($Script:QuestionCounter)'>`n"
            $Script:xmlText += "<questionText>"
            $Script:xmlText += $QuestionText
            $Script:xmlText += "</questionText>`n"
            $SB.Invoke()
            $Script:xmlText += "</question>`n"
        }
        Yaml {
            $Script:ymlText += "Question$($Script:QuestionCounter):`n"
            $Script:ymlText += "  questionText:$QuestionText"
            $Script:ymlText += "`n"
            $SB.Invoke()
        }
    }
}

function Option
{
    param([String]$Text, [Switch]$Answer)
    $Script:OptionCounter++
    # Welcher Modus?
    switch($Script:Outputmode)
    {
        "Xml" {
            if ($Answer) {
                $Script:xmlText += "<option id='$($Script:OptionCounter)' answer='true'>$Text</option>`n"
            } else {
                $Script:xmlText += "<option id='$($Script:OptionCounter)'>$Text</option>`n"
            }
        }
        "Yaml" {
            $Script:ymlText += "  option$($Script:OptionCounter):$Text`n"
            if ($Answer) {
                $Script:ymlText += "    answer:True`n"
            }
        }
    }
}
