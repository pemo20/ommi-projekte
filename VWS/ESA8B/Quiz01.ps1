<#
 .SYNOPSIS
 Definition of a simple Quiz with a DSL
#>

$psm1Path = Join-Path -Path $PSScriptRoot -ChildPath ".\PoshQuizDSL.psm1"
Import-Module -Name $psm1Path -Force

Quiz -Id Posh1 -Title "PowerShell Quiz 1" -Description "A very simple Quiz about PowerShell" -Level A {
    
    Question "Who was the inventor of PowerShell?" {
        Option "Bill Gates"
        Option "Mark Russinowich"
        Option "Jeffrey Snower" -Answer
        Option "Charles Petzold"
    }

    Question "When was version 1.0 released?" {
        Option "7.12.1996"
        Option "31.12.1999"
        Option "1.11.2006" -Answer
        Option "25.10.2022"
    }
    
} # -AsYaml