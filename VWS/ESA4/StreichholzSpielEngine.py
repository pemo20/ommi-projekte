#! env/bin/python3
# -*- coding:utf-8 -*-
# Definiert die Funktionalität zur Festlegung der Gewinnstrategie

# Definiert die Methode getStrategie zur Festlegung einer Gewinnstrategie für den Computer
class SPEngine:

    # Aufstellen der Gewinnstrategie in Abhängigkeit der Anzahl der Streichhölzer
    def __init__(self, anzahlHoelzer):
        # Liste für jedes Streichholz mit "" vorbelegen
        self.gStrategie = ["" for _ in range(anzahlHoelzer+1)]
        # Die ersten drei Felder vorbelegen
        self.gStrategie[1] = "Nein"
        self.gStrategie[2] = "Ja"
        self.gStrategie[3] = "Ja"
        # Laufvariable beginnt aufgrund der Vorbelegung mit 3
        i = 3
        while i < anzahlHoelzer:
            i += 1
            if self.gStrategie[i-1] == "Ja" and self.gStrategie[i-2] == "Ja" and self.gStrategie[i-3] == "Ja":
                self.gStrategie[i] = "Nein"
            else:
                self.gStrategie[i] = "Ja"
        return self.gStrategie

    # Die die Anzahl der Hölzer für den Computerzug zurück    
    def getComputerZug(self, anzahl) -> int:
        if anzahl > 3:
            if self.gStrategie[anzahl] == "Ja":
                anzahlComputer = 1
            elif self.gStrategie[anzahl-1] == "Ja":
                anzahlComputer = 2
            else:
                anzahlComputer = 3
        else:
            anzahlComputer = anzahl - 1
        return anzahlComputer



