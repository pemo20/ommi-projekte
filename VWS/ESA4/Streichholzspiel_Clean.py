#! env/bin/python3
# -*- coding:utf-8 -*-
# Umsetzung des Streichholzspiels mit der Anwendung von Standard-Clean Code-Regeln
# Erstellt: 03/12/23
# Peter Monadjemi (MatrikelNr 7004123) - ESA4 für Modul VWS SS 23/24

from StreichholzSpielEngine import SPEngine
from EinzahlMehrzahlConverter import EMConverter

# Startet eine Spielrunde mit einer vorgegebenen Anzahl an Spielhölzern
def StarteSpiel(anzahlHoelzer) -> None:
    # Gewinnstrategie für den Computer festlegen in Abhängigkeit der Anzahl der Spielhölzer
    spEngine = SPEngine(anzahlHoelzer)
    # Instanz bilden (offiziel kennt Python 3.11 keine statischen Methoden)
    emConverter = EMConverter()
    anzahlAktuell = anzahlHoelzer
    exitFlag = False
    while(not exitFlag):
        streichholzWort = emConverter.getEinzahlMehrzahlWort("Streichholz", anzahlAktuell)
        streichholzAnzahlWort = emConverter.getEinzahlMehrzahlWort("Liegen", anzahlAktuell)
        print(f"Es {streichholzAnzahlWort} {anzahlAktuell} {streichholzWort} auf dem Tisch - wie viele Streichhölzer möchtest du wegnehmen? (1, 2 oder 3)")
        anzahl = int(input())
        # Stimmt die Eingabe?
        if anzahl < 1 or anzahl > 3:
            print("Ungültige Eingabe - gib 1,2 oder 3 ein!")
            continue
        if anzahl > anzahlAktuell:
            print("Ungültige Eingabe - es liegen nicht so viele Streichhölzer auf dem Tisch!")
            continue
        # Aktuelle Anzahl reduzieren
        anzahlAktuell -= anzahl
        # Hat der Spieler gewonnen?
        if anzahlAktuell == 1:
            print("Du hast gegen den Computer gewonnen, denn es liegt nur noch ein Streichholz auf dem Tisch!")
            break
        streichholzWort1 = emConverter.getEinzahlMehrzahlWort("Streichholz", anzahl)
        streichholzWort2 = emConverter.getEinzahlMehrzahlWort("Streichholz", anzahlAktuell)
        streichholzAnzahlWort = emConverter.getEinzahlMehrzahlWort("sind", anzahlAktuell)
        print(f"Du hast {anzahl} {streichholzWort1} gezogen - es {streichholzAnzahlWort} noch {anzahlAktuell} {streichholzWort2} übrig.")
        # Der Computer zieht die Anzahl an Hölzern nach der festgelegten Strategie
        anzahlComputer = spEngine.getComputerZug(anzahlAktuell)
        # Aktuelle Anzahl reduzieren
        anzahlAktuell -= anzahlComputer
        streichholzWort1 = emConverter.getEinzahlMehrzahlWort("Streichholz", anzahlComputer)
        streichholzWort2 = emConverter.getEinzahlMehrzahlWort("Streichholz", anzahlAktuell)
        streichholzAnzahlWort = emConverter.getEinzahlMehrzahlWort("sind", anzahlAktuell)
        print(f"Der Computer zieht {anzahlComputer} {streichholzWort1} - es {streichholzAnzahlWort} noch {anzahlAktuell} {streichholzWort2} übrig.")
        # Hat der Spieler verloren?
        if anzahlAktuell == 1:
            print("Es liegt nur noch ein Streichholz auf dem Tisch, Du hast verloren!")
            break

# Einsprungspunkt des Programms
if __name__ == "__main__":
    # Anzahl der Spielhölzer
    ANZAHL_HOELZER = 18
    StarteSpiel(ANZAHL_HOELZER)
