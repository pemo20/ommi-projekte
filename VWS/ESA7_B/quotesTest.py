#! env/bin/python3
# file: quotesTest.py
import yaml
from os import path
import random

ymlPath = path.join(path.dirname(__file__), "quotes.yml")

with open(ymlPath, encoding="utf8") as fh:
    quoteDic = yaml.safe_load(fh)

with open(ymlPath, encoding="utf8") as fh:
    lines = [line.strip() for line in fh.readlines()]
    
# print(quoteDic.keys())

quoteDic2 = {}
lastKey = ""
for line in lines:
    if line.strip() == "":
       continue
    if line.endswith(":"):
       lastKey = line[:-1].strip()
       print(lastKey)
       quoteDic2[lastKey] = []
    else:
       quoteDic2[lastKey].append(line)

# print(quoteDic2)

lastKey = "STOS"
z = random.randint(0, len(quoteDic2[lastKey]) -1)
print(quoteDic2[lastKey][z])